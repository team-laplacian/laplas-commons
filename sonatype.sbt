
publishMavenStyle := true

licenses := Seq("MIT" -> url("http://opensource.org/licenses/MIT"))

homepage := Some(url("https://gitlab.com/team-laplacian/laplas-commons"))
scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/team-laplacian/laplas-commons"),
    "scm:git@gitlab.com:team-laplacian/laplas-commons.git"
  )
)
developers := List(
  Developer(id="mfranic", name="Mario Franic", email="mario@laplacian.hr", url=url("https://gitlab.com/mfranic")),
  Developer(id="bmarinovic", name="Blaz Marinovic", email="blaz@laplacian.hr", url=url("https://gitlab.com/bmarinovic"))
)
