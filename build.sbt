import sbt.Keys.libraryDependencies

name := "laplas-commons"

organization := "hr.laplacian.laplas"

scalaVersion := "2.12.8"
crossScalaVersions := Seq("2.11.12", "2.12.8")
scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "javax.inject" % "javax.inject" % "1",
  "hr.laplacian.laplas" %% "laplas-redis" % "1.0.2",
  "com.typesafe.play" %% "play-json" % "2.7.3",
  "org.typelevel" %% "cats-core" % "2.0.0"
)
publishTo := sonatypePublishTo.value

import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  releaseStepCommand(
    """sonatypeOpen "hr.laplacian.laplas" "Laplas Staging"""".stripMargin
  ),
  releaseStepCommand("publishSigned"),
  setNextVersion,
  commitNextVersion,
  releaseStepCommand("sonatypeReleaseAll"),
  pushChanges
)
