package hr.laplacian.laplas.commons

package object error {
  type Eor[T] = Either[UuidError, T]
}
