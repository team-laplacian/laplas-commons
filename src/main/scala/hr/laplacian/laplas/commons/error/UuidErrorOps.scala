package hr.laplacian.laplas.commons.error

trait UuidErrorOps {
  implicit class StringToErrorOps(error: String) {
    def toUuidError: UuidError = SimpleUuidError(error)
    def toUuidError(uuid: String): UuidError = SimpleUuidError(error, uuid)

    def toUuidError(uuidOpt: Option[String]): UuidError =
      if (uuidOpt.isDefined) toUuidError(uuidOpt.get) else toUuidError
  }

  implicit class ThrowableToErrorOps(throwable: Throwable) {
    def toUuidError: UuidError = ThrowableUuidError(throwable)
    def toUuidError(uuid: String): UuidError =
      ThrowableUuidError(throwable, uuid)

    def toUuidError(uuidOpt: Option[String]): UuidError =
      if (uuidOpt.isDefined) toUuidError(uuidOpt.get) else toUuidError
  }
}
object UuidErrorOps extends UuidErrorOps
