package hr.laplacian.laplas.commons.error

import cats.data.EitherT
import cats.implicits._

import scala.concurrent.{ ExecutionContext, Future }

trait EorOps {

  implicit class FutureEorOps[T](eor: Future[Eor[T]])(implicit val ec: ExecutionContext) {

    def tap[U](f: T => U): Future[Eor[T]] = EitherT(eor).flatTap(t => EitherT(f(t).toFutureEor)).value
  }

  implicit class ToEorEOps(e: UuidError) {
    def toEor[T]: Eor[T]               = e.asLeft[T]
    def toFutureEor[T]: Future[Eor[T]] = Future.successful(e.asLeft[T])
  }

  implicit class ToEorOps[T](t: T) {
    def toEor: Eor[T]               = t.asRight[UuidError]
    def toFutureEor: Future[Eor[T]] = Future.successful(t.asRight[UuidError])
  }
}

object EorOps extends EorOps
