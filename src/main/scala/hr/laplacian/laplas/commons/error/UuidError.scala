package hr.laplacian.laplas.commons.error

trait UuidError {
  def message: String
  def uuid: String
  def description: String
  def throwable: Throwable
  def throwException(): Nothing
}
