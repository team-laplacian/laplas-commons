package hr.laplacian.laplas.commons.error

import java.util.UUID

case class SimpleUuidError(
    message: String,
    uuid: String = UUID.randomUUID.toString
) extends UuidError {
  val description = s"[$uuid] $message"
  val throwable = new RuntimeException(description)

  def throwException(): Nothing = throw throwable
}
