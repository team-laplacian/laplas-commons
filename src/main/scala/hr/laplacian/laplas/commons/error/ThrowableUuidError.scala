package hr.laplacian.laplas.commons.error

import java.io.{PrintWriter, StringWriter}
import java.util.UUID

case class ThrowableUuidError(
    throwable: Throwable,
    uuid: String = UUID.randomUUID.toString
) extends UuidError {

  val message: String = throwable.getMessage

  val description: String = {
    val stringWriter = new StringWriter
    throwable.printStackTrace(new PrintWriter(stringWriter))
    s"""
       |[$uuid] $message
       | ${stringWriter.toString}
       """.stripMargin
  }

  override def throwException(): Nothing = throw throwable
}
