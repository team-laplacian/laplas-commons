package hr.laplacian.laplas.commons.processwatcher

import java.time.Instant

import hr.laplacian.laplas.commons.json.EnumJsonFormat
import hr.laplacian.laplas.commons.processwatcher.ProcessWatcherService.ProcessResult
import hr.laplacian.laplas.redis.RedisService
import javax.inject.Inject
import play.api.libs.json.{Json, OFormat}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

trait ProcessWatcherService
{
  def watch[T](processKey: ProcessWatcherService.ProcessKey)(block: => ProcessResult[T]): T
  def watchF[T](processKey: ProcessWatcherService.ProcessKey)(block: => Future[ProcessResult[T]])(implicit executionContext: ExecutionContext): Future[T]

  def getProcessWatchInfo(processKey: ProcessWatcherService.ProcessKey): Option[ProcessWatcherService.ProcessWatchInfo]
}

object ProcessWatcherService extends EnumJsonFormat
{
  case class ProcessKey
  (
    referenceId: String,
    referenceType: String,
    actionType: String
  )
  {
    def value: String = s"process-watch-$actionType-$referenceType-$referenceId"
  }

  case class ProcessResult[T]
  (
    isSuccess : Boolean,
    result : T
  )

  case class ProcessWatchInfo
  (
    status: ProcessStatus,
    startedAt: Long,
    finishedAt: Option[Long] = None
  )

  implicit val processTrackerStatusJsonFormat: OFormat[ProcessWatchInfo] = Json.format[ProcessWatchInfo]
}

class ProcessWatcherServiceImpl @Inject()
(
  val redisService: RedisService

) extends ProcessWatcherService
{
  protected val DEFAULT_PROCESS_TTL: FiniteDuration = 24.hours

  override def watch[T](processKey: ProcessWatcherService.ProcessKey)(block: => ProcessResult[T]): T = {
    val processWatchInfo = startWatch(processKey)

    val processResult = block

    finishWatch(processKey, processWatchInfo, processResult)
  }

  override def watchF[T](processKey: ProcessWatcherService.ProcessKey)(block: => Future[ProcessResult[T]])(implicit executionContext: ExecutionContext): Future[T] = {
    val processWatchInfo = startWatch(processKey)

    val processResult = block

    processResult.map(finishWatch(processKey, processWatchInfo, _))
  }

  override def getProcessWatchInfo(processKey: ProcessWatcherService.ProcessKey): Option[ProcessWatcherService.ProcessWatchInfo] = {
    redisService
      .get(processKey.value)
      .flatMap(processData => Json.parse(processData).validate[ProcessWatcherService.ProcessWatchInfo].asOpt)
  }

  private def startWatch(processKey: ProcessWatcherService.ProcessKey): ProcessWatcherService.ProcessWatchInfo = {
    val processWatchInfo = ProcessWatcherService.ProcessWatchInfo(status = ProcessStatus.PENDING, startedAt = Instant.now().getEpochSecond)

    redisService.setEx(
      processKey.value,
      Json.toJson(processWatchInfo).toString(),
      DEFAULT_PROCESS_TTL
    )

    processWatchInfo
  }

  private def finishWatch[T](processKey: ProcessWatcherService.ProcessKey, processWatchInfo: ProcessWatcherService.ProcessWatchInfo, processResult : ProcessResult[T]): T = {
    if(processResult.isSuccess){
      redisService.set(
        processKey.value,
        Json.toJson(processWatchInfo.copy(status = ProcessStatus.COMPLETED, finishedAt = Some(Instant.now().getEpochSecond))).toString()
      )
    } else {
      redisService.set(
        processKey.value,
        Json.toJson(processWatchInfo.copy(status = ProcessStatus.FAILED, finishedAt = Some(Instant.now().getEpochSecond))).toString()
      )
    }

    processResult.result
  }
}
