package hr.laplacian.laplas.commons.processwatcher;

public enum ProcessStatus
{
    PENDING,
    COMPLETED,
    FAILED
}
