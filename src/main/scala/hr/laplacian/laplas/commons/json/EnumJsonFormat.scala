package hr.laplacian.laplas.commons.json

import play.api.libs.json._

import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

trait EnumJsonFormat {
  implicit def jsonFormat[TEnum <: Enum[TEnum]](
      implicit classTag: ClassTag[TEnum]): Format[TEnum] = new Format[TEnum] {
    override def writes(value: TEnum): JsValue = JsString(value.name)

    override def reads(json: JsValue): JsResult[TEnum] = {
      val enumClass = classTag.runtimeClass.asInstanceOf[Class[TEnum]]
      Try(Enum.valueOf[TEnum](enumClass, json.as[String])) match {
        case Success(enumValue) => JsSuccess(enumValue)
        case Failure(error)     => JsError(error.getMessage)
      }
    }
  }
}

object EnumJsonFormat extends EnumJsonFormat
